#!/bin/bash

umask 000

# VARIABLES
BASE_PATH= # WHERE YOU SAVED THE PROJECT
PROJECT_PATH=$BASE_PATH/template-filler-py
VENV_PATH=$PROJECT_PATH/venv/bin
START_TIME=$(date +'%y-%m-%d %T')

if [ ! -d "$PROJECT_PATH/venv" ]; then
  # Control will enter here if $PROJECT_PATH/venv doesn't exist.
  echo "$(date +'%y-%m-%d %T')INFO [$$] - Python virtual environment does not exit..."
  echo "$(date +'%y-%m-%d %T')INFO [$$] - Creating Python virtual environment..."
  python3 -m venv "$PROJECT_PATH/venv"
  echo "$(date +'%y-%m-%d %T')INFO [$$] - Activating Python virtual environment..."
  source $VENV_PATH/activate
  echo "$(date +'%y-%m-%d %T')INFO [$$] - Installing dependencies from requirements.txt"
  cd $PROJECT_PATH; pip3 install -r requirements.txt
fi

# Activate venv Python
echo "$(date +'%y-%m-%d %T')INFO [$$] - Activating Python virtual environment..."
source $VENV_PATH/activate
rc=$?

if [ $rc -ne 0 ]; then
	echo "$(date +'%y-%m-%d %T') ERROR [$$] - unable to activate Python virtual environment: $rc."
else
	echo "$(date +'%y-%m-%d %T') INFO [$$] - Python virtual environment has been activated successfully."
	echo "$(date +'%y-%m-%d %T') INFO [$$] - Running Python script..."
	echo ""

	############ EXECUTE SCRIPT #############

	cd $PROJECT_PATH

	python3 -m templatefillerpy "$@"
	py_rc=$?

	if [ $py_rc -ne 0 ]; then
		echo ""
		echo "$(date +'%y-%m-%d %T') ERROR [$$] - Python has been completed with errors: $py_rc."
	else
		echo ""
		echo "$(date +'%y-%m-%d %T') INFO [$$] - Python has been completed successfully."
	fi

	echo "$(date +'%y-%m-%d %T') INFO [$$] - Deactivating Python virtual environment..."
	deactivate
	rc=$?

	if [ $rc -ne 0 ]; then
		echo "$(date +'%y-%m-%d %T')ERROR [$$] - unable to deactivate Python virtual environment: $rc."
	else
		echo "$(date +'%y-%m-%d %T') INFO [$$] - Python virtual environment has been deactivated successfully."
	fi

	############ EXIT: PYTHON RETURN CODE #############
	exit $py_rc
fi
