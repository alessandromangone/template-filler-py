v1.0.2

# template-filler-py
A tool made with Python and openpyxl to insert raw data in xlsx format inside a custom Excel template using a txt file as guideline.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
    * [Project structure](#project-structure)
    * [How to write your txt file](#how-to-write-your-txt-file)
        - [Insert a comment](#insert-a-comment)
        - [Add dynamic references](#add-dynamic-references)
        - [Summary](#summary)
    * [How to run the sample](#how-to-run-the-sample)
* [Features](#setup)
* [Contributing](#contributing)
* [Acknowledgements](#acknowledgements)
* [Changelog](#changelog)
    

## General info

The goal of this project is to provide a useful tool to insert several xlsx files and/or sheets within one or multiple custom templates.
The final result will be a terminal executable bash script that doesn't need Python knowledge and can be used by anyone following a few simple rules.

## Technologies
The tool is created with:
- Python >= 3.0
- openpyxl >= 3.0.5 (https://openpyxl.readthedocs.io/en/stable/)

**N.B.**: The script can run with Python 2.7 also, but you have to manually install dependecies and virtualenv with pip in order to execute it.

## Setup
First of all, be sure to have a running version of Python >= 3 and PIP. If not check this links to install the lastest version based on your OS: 
- Python website (https://www.python.org/downloads/)
- A guide to install PIP (https://www.makeuseof.com/tag/install-pip-for-python/)

Now you can download the zip folder project directly from GitLab (see the figure below) 

![GitLab download button](https://i.imgur.com/FktPQt7.png)

or clone the repository through the command.
(If you don't have git comand and you want to install it chech this link: [install Git](https://git-scm.com/))
```bash
git clone https://gitlab.com/alessandromangone/template-filler-py.git
```

Once everything is ready, yuo have to install necessary dependecies. To do so, in the terminal go into the project folder and type the command:

```bash
cd where-you-save-the-path-project
pip3 install -r requirements.txt
```

To execute the code you have to type one of the following commands:
```bash
python3 -m templatefillerpy path-of-your-txt/guideline.txt 
```

If your txt guideline has dynamic variables that have to be replaced with specific values you have to pass them through the command
```bash
python3 -m templatefillerpy path-of-your-txt/guideline.txt --variables referenceDate=202005 var2=val2 
```

To specify dynamic variables in your txt guideline read [here](#add-dynamic-references)

### Project structure
The project is structured as follow:

```bash
template-filler-py/
├── CHANGELOG.md
├── LICENSE.md
├── Makefile
├── README.md
├── bin
│   └── templatefiller.sh
├── logs
├── requirements.txt
└── templatefillerpy
    ├── __init__.py
    ├── __main__.py
    ├── helpers.py
    ├── sample
    │   ├── helpers.py
    │   ├── sample.txt
    │   ├── templatefillerpy.py
    │   └── xlsx
    │       ├── Sample100.xlsx
    │       ├── sample.xlsx
    │       └── template.xlsx
    └── templatefillerpy.py
```
in the **sample/** folder you can find a complete example of the tool. As you can see there is a folder called **xlsx/** where you can store your *xlsx files* (they can be saved anywhere you want, the important thing is to write the path correctly in the txt file as we will see).

In the **bin/** folder you can find an executable bash script that allows you to create a virtual environment, install requirements and execute the python script. In order to be able to execute the script you have to open the .sh file and add your project path.

In the **logs/** folder you will find the logs corresponding the timestamp you run the tool. If something went wrong you can analyze the log to check errors.

```bash
#VARIABLES
BASE_PATH= # WHERE YOU SAVED THE PROJECT
PROJECT_PATH=$BASE_PATH/template-filler-py
VENV_PATH=$PROJECT_PATH/venv/bin
```

After that to run the script, go in the project folder and launch the command:

```bash
./bin/templatefiller.sh path-of-your-txt/guideline.txt (--variables var1=val2 var2=val2 ...)*
```

*--variables is an optional command


### How to write your txt file
Below you can check how *sample.txt* is written. As you can see the order is not important, the only thing that matters is that **each key** is present (in case of error that instruction will not be executed)
```
INPUT=xlsx/sample.xlsx # where you are stored your xlsx file
OUTPUT=xlsx/template.xlsx # where you are stored your xlsx template
IN_SHEET=Sheet1 # the sheet you want to use
OUT_SHEET=Order Sheet # the template's destination sheet 
IN_RANGE=A2:G44 # the range of cells to copy
OUT_RANGE=C18:I60 # where to paste the input cells

INPUT=xlsx/Sample100.xlsx
OUTPUT=xlsx/template.xlsx
IN_RANGE=A2:E51
OUT_RANGE=A4:E53
IN_SHEET=Sheet1
OUT_SHEET=Sample_Data
```

### Insert a comment

To add comment in your txt to separate instructions and organize them better. Below an example of comment inside the txt guideline

```
INPUT=xlsx/sample.xlsx
OUTPUT=xlsx/template.xlsx
IN_SHEET=Sheet1
OUT_SHEET=Order Sheet
IN_RANGE=A2:G44
OUT_RANGE=C18:I60

// This is a comment. 

INPUT=xlsx/Sample100.xlsx
OUTPUT=xlsx/template.xlsx
IN_RANGE=A2:E51
OUT_RANGE=A4:E53
IN_SHEET=Sheet1
OUT_SHEET=Sample_Data

// This is a
// multi-line comment 

```

### Add dynamic references
Sometimes it may happen that you have the same report that runs monthly and the xlsx files it refers to are referenced by date (e.g. 202005). To avoid having to update the guideline every time, the possibility to indicate the variables to be replaced inside the txt has been added. The variables to replace **must** start with @. ***Python is case sensitive*** so pay attention to how they are written and how you will then pass them to the program.

Look at the example below.

```
INPUT=xlsx/sample_@referenceDate.xlsx
OUTPUT=xlsx/template.xlsx
IN_SHEET=Sheet1
OUT_SHEET=Order Sheet
IN_RANGE=A2:G44
OUT_RANGE=C18:I60
```

### Summary
The txt guideline **must** follow the guidelines below:
* Each key must be declared
* Every instruction must be separated by a newline ("\n")
* Every comment line must be start with "//"
* The dynamic reference must be start with "@" to be easy to find and replace

### How to run the sample
In order to run the sample you have to open your terminal and navigate into the project path through the *cd* command.
Once you are in the project folder run the following commands.

```bash
cd templatefillerpy/sample
python templatefillerpy.py sample.txt 

# if you have both Python version
python3 templatefillerpy.py sample.txt 

## WINDOWS USER ##
cd templatefillerpy\sample

python templatefillerpy.py sample.txt 

# if you have both Python version
python3 templatefillerpy.py sample.txt 
```

![GIF sample](https://i.imgur.com/JEvhS7e.gif)

In alternative, you can run the bash script as follow (OSX and Unix only):

![GIF sample2](https://i.imgur.com/nJDZDxB.gif)

```bash
./bin/templatefiller.sh templatefillerpy/sample/sample.txt
```

The program converts your txt file in a json structure easy to navigate and get the fields required to execute the instructions.
After the run an *sample.log* file will be generated, if something went wrong you can check the log file to see which error has occured.

## Features

[v1.0.2]
- Create bash script into bin/ folder to embed the python script inside other programs
- Now you can run the script with the bash script ./bin/templatefiller.sh that will create virtual environment and install all dependencies

[v1.0.1]
* Add comments in your txt guideline using "//"
* Pass variables to dynamic file reference

[v1.0.0]
* Copy raw xlsx table into a custom template using a txt file as guideline
* A Log to check what went wrong
* Easy to use, no Python knowledge necessary, just follow the instruction and enjoy the result
* Process multiple files and templates using only one txt file

### To do:
* Could be cool using different format like .csv, .tbs, .sas7bdat, SQL-Tables without generate tons of excel file. Maybe use CLI interface to specify which format are the data and think about different txt guidelines. Tables can be uploaded in Python, manipulated with Pandas and then copy the values in the right destination cells.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Acknowledgements
To copy and paste excel cells in the right place I have followed this guide, thank you *Yagisanatode*:
[link](https://yagisanatode.com/2017/11/18/copy-and-paste-ranges-in-excel-with-openpyxl-and-python-3/)

## Changelog
> You can check out the full changelog [here](https://gitlab.com/alessandromangone/template-filler-py/-/blob/master/CHANGELOG.md)

## License
>You can check out the full license [here](https://gitlab.com/alessandromangone/template-filler-py/-/blob/master/LICENSE.md)

This project is licensed under the terms of the **MIT** license.