import codecs
import json
import logging
import os
import uuid

logging.basicConfig(filename='sample.log', filemode='a+', format='%(asctime)s - %(process)d - %(levelname)s - %(message)s',
                    level=logging.DEBUG)

from openpyxl.utils.cell import coordinate_from_string, column_index_from_string


def txt_to_json(filename):
    # check if filename exists
    absolutepath = os.path.abspath(filename)
    if os.path.exists(absolutepath):
        isacomment = False
        json_dict = {}
        obj = {}
        with codecs.open(absolutepath, 'r') as file:

            uid = str(uuid.uuid4())
            for line in file:
                # every instruction should be separated by a newline
                if line != '\n':
                    # // indicate that there is a comment to structured well the txt (many instructions)
                    if not line.startswith("//"):
                        key, value = line.strip().split('=')
                        obj[key] = value.strip()
                        json_dict[uid] = obj
                        isacomment = False
                    else:
                        isacomment = True
                        continue
                else:
                    if not isacomment:
                        uid = str(uuid.uuid4())
                        obj = {}

        outfile = filename.split('.txt')[0]
        output = outfile + '.json'
        out_file = open(output, 'w')
        json.dump(json_dict, out_file, indent=4)
        out_file.close()

        return output

    else:
        logging.error("File {} not found".format(filename))
        raise Exception("File not found.")


def update_json_with_variables(jsonfile, *args):
    with open(jsonfile, 'r') as file:
        data = file.read()

    obj = json.loads(data)
    variables = {}
    for arg in args:
        key, value = arg.strip().split('=')
        variables[key] = value.strip()
    obj["variables"] = variables
    out_file = open(jsonfile, 'w')
    json.dump(obj, out_file, indent=4)
    out_file.close()


def replace_variables(target, **variables):
    for key, val in variables.items():
        target = target.replace("@" + key, val)
    return target


def get_range_from_cell(cell):
    coord = coordinate_from_string(cell)
    col = column_index_from_string(coord[0])
    row = coord[1]

    return col, row


# https://yagisanatode.com/2017/11/18/copy-and-paste-ranges-in-excel-with-openpyxl-and-python-3/

# Copy range of cells as a nested list
# Takes: start cell, end cell, and sheet you want to copy from.
def copyRange(startCol, startRow, endCol, endRow, sheet):
    rangeSelected = []
    # Loops through selected Rows
    for i in range(startRow, endRow + 1, 1):
        # Appends the row to a RowSelected list
        rowSelected = []
        for j in range(startCol, endCol + 1, 1):
            rowSelected.append(sheet.cell(row=i, column=j).value)
        # Adds the RowSelected List and nests inside the rangeSelected
        rangeSelected.append(rowSelected)

    return rangeSelected


# Paste range
# Paste data from copyRange into template sheet
def pasteRange(startCol, startRow, endCol, endRow, sheetReceiving, copiedData):
    countRow = 0
    try:
        for i in range(startRow, endRow + 1, 1):
            countCol = 0
            for j in range(startCol, endCol + 1, 1):
                sheetReceiving.cell(row=i, column=j).value = copiedData[countRow][countCol]
                countCol += 1
            countRow += 1
    except IndexError as ind_err:
        print("IndexError: list index out of range. Output excel range is too much bigger than the input range.")
        print("Please check your txt file.")
        logging.error("IndexError: list index out of range. Output excel range is too much bigger than the "
                      "input range.")
