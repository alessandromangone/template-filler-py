#!/usr/bin/env python

import logging
import json
import openpyxl
import argparse
import sys
import os

# external module
import helpers

logging.basicConfig(filename='sample.log', filemode='w', format='%(asctime)s - %(process)d - %(levelname)s - %(message)s',
                    level=logging.DEBUG)


def filler(replace=False, **kwargs):
    logging.info("{} elements will be process.".format(len(kwargs)))
    for i, key in enumerate(kwargs.keys()):
        if key != "variables":
            try:
                # get input file
                in_file = os.path.abspath(kwargs[key]["INPUT"])
                print(in_file)
                # get template file
                out_file = os.path.abspath(kwargs[key]["OUTPUT"])
                # get input sheet
                in_sheet = kwargs[key]["IN_SHEET"]
                # get output sheet
                out_sheet = kwargs[key]["OUT_SHEET"]
                # get in_range and split to pass to get_range_from_cell function
                in_cell_start = kwargs[key]["IN_RANGE"].split(":")[0]
                in_cell_end = kwargs[key]["IN_RANGE"].split(":")[1]
                # get out_range and split to pass to get_range_from_cell function
                out_cell_start = kwargs[key]["OUT_RANGE"].split(":")[0]
                out_cell_end = kwargs[key]["OUT_RANGE"].split(":")[1]

                # get number range for input range to start to store values inside cells
                in_cell_start_col, in_cell_start_row = helpers.get_range_from_cell(in_cell_start)
                in_cell_end_col, in_cell_end_row = helpers.get_range_from_cell(in_cell_end)

                # get number range for output range to start to store values inside cells
                out_cell_start_col, out_cell_start_row = helpers.get_range_from_cell(out_cell_start)
                out_cell_end_col, out_cell_end_row = helpers.get_range_from_cell(out_cell_end)

                print("Input File={}, Output File={}, Input Sheet={}, Output Sheet={}".format(in_file, out_file, in_sheet,
                                                                                              out_sheet))
                logging.info("Instruction {} is well formatted.".format(i + 1))
                logging.debug(
                    "Input File={}, Output File={}, Input Sheet={}, Output Sheet={}".format(in_file, out_file, in_sheet,
                                                                                            out_sheet))

                try:
                    if replace:
                        in_file = helpers.replace_variables(in_file, **kwargs["variables"])
                        out_file = helpers.replace_variables(out_file, **kwargs["variables"])
                    # File to be copied
                    wb = openpyxl.load_workbook(in_file)  # Add file name
                    sheet = wb[in_sheet]  # Add Sheet name
                    selected_range = helpers.copyRange(in_cell_start_col, in_cell_start_row, in_cell_end_col,
                                                       in_cell_end_row, sheet)
                    # File to be pasted into
                    template = openpyxl.load_workbook(out_file)  # Add file name
                    temp_sheet = template[out_sheet]  # Add Sheet name
                    helpers.pasteRange(out_cell_start_col, out_cell_start_row, out_cell_end_col,
                                       out_cell_end_row, temp_sheet, selected_range)
                    # You can save the template as another file to create a new file here too.
                    template.save(out_file)
                    print("Range copied and pasted!")
                    logging.debug("Range copied and pasted!")
                except FileNotFoundError as fnf_error:
                    print(fnf_error)
                    logging.error("{}. Error in the {} instruction.".format(fnf_error, i + 1))
                # except Exception as invalid_format:
                #     logging.warning("Exception: openpyxl does not support the old .xls file format, please use xlrd to read"
                #                     " this file, or convert it to the more recent .xlsx file format.")

            except KeyError as key_err:
                logging.error(
                    "KeyError: {} at instruction {}. Check if the key is missing or misspelled in your txt file.".format(
                        key_err, i + 1))
                print("KeyError: {} at instruction {}. Check if the key is missing or misspelled in your txt file.".format(
                    key_err, i + 1))


def run(filename, *args):
    print("Converting txt to json...")
    logging.debug("Converting {} to json".format(filename))
    output = helpers.txt_to_json(filename)
    if len(args) > 0:
        helpers.update_json_with_variables(output, *args)
        logging.debug("Output file is {}".format(output))

        with open(output, 'r') as file:
            data = file.read()

        obj = json.loads(data)

        if len(obj) < 1:
            logging.error("The json contains 0 element. Something went wrong. Check your txt file.")
            raise Exception("No element founded. Check yuor txt file.")
        else:
            logging.info("Starting to fill the template.")
            print("Starting to fill the template...")
            filler(replace=True, **obj)
            print("Done. Bye!")
    else:
        logging.debug("Output file is {}".format(output))

        with open(output, 'r') as file:
            data = file.read()

        obj = json.loads(data)

        if len(obj) < 1:
            logging.error("The json contains 0 element. Something went wrong. Check your txt file.")
            raise Exception("No element founded. Check yuor txt file.")
        else:
            logging.info("Starting to fill the template.")
            print("Starting to fill the template...")
            filler(**obj)
            print("Done. Bye!")


def main():
    cli = argparse.ArgumentParser(description='List the content of a folder')
    cli.add_argument('File',
                     metavar='file',
                     type=str,
                     help='the reference txt file')

    cli.add_argument('--variables',
                     action='store',
                     nargs='*',
                     required=False,
                     default=[])

    args = cli.parse_args()
    txt_file = args.File
    if not os.path.exists(txt_file):
        print('The file specified does not exist')
        sys.exit()

    logging.info("Process started.")
    run(txt_file, *args.variables)
    logging.info("Process ended.")


if __name__ == '__main__':
    main()
