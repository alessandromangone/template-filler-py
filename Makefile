
install: venv
	source ./venv/bin/activate && pip3 install -r requirements.txt

remove-env:
	rm -rf venv

venv:
	: # Create venv if it doesn't exist
	test -d venv || python3 -m venv venv

test:
	cd templatefillerpy/sample/; python3 templatefillerpy.py sample.txt

clean-test:
	rm -rf templatefillerpy/sample/__pycache__
	rm templatefillerpy/sample/app.log
	rm templatefillerpy/sample/sample.json

clean-log:
	rm log/*.log

clean:
	rm -rf templatefillerpy/__pycache__